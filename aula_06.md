# Aula 06 - Quicksort

## Material

1. [Quicksort](https://gitlab.com/ds143-alexkutzke/material/-/blob/main/06/00_quick/quicksort.pdf);
2. [Implementações](https://gitlab.com/ds143-alexkutzke/material/-/tree/main/06/codes);
