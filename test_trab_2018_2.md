# Teste de desempenho dos trabalhos

Os trabalhos escolhidos para os testes de desempenho 
foram aqueles que implementaram corretamente todas as 
funcionalidade descritas na especificação do trabalho.

Testes com `--freq 10`.

Cada trabalho foi executado 5 vezes para cada uma das entradas. O menor tempo
registrado foi escolhido.

## Resultado

|#|Grupo                          |Turma|64mb|128mb|256mb| 512mb|
|-|:------------------------------|:---:|:--:|:---:|:---:|:----:|
|1|bianca_daniel_leonardorocha    |T    |2.19s| 4.48s| 8.98s| 17.46s|
|2|filipe                         |T    |4.19s| FAIL| FAIL|  FAIL|
|3|luiz_lucasantonio_marco_matheus|T    |2.27s| 4.59s| 9.07s| 18.51s|
|4|thiago_adriel_luiz             |N    |3.07s| 6.41s|11.96s|134.93s|

## Conclusão

O trabalho 1 feito pelos alunos "Bianca Franco, Daniel Machado e Leonardo Rocha"
apresentou o melhor desempenho nos testes realizados. Por isso, receberá
**2 pontos extras** em sua nota (melhora da turma e melhor disciplina).

O trabalho 4 feito pelos alunos "Adriel Barbosa, Luiz Otávio e Thiago Cezario"
receberá **1 ponto extra** em sua nota (melhor da turma).

O trabalho 3 feito pelos alunos "Luiz Dominico, Lucas Antonio, Marco Aurélio e 
Mateus Mendes" receberá **1 ponto extra** em sua nota por ter tido um empate
técnico com o trabalho 1.
